const { Client } = require("pg");

const client = new Client({
  user: "Admin",
  host: "localhost",
  database: "todo-app",
  password: "123456",
  port: 5432,
});

client
  .connect()
  .then(() => console.log(".......Database connected........."))
  .catch((err) => console.error("connection error", err.stack));

// client
//   .query("DELETE FROM todos")
//   .then((res) => console.log("Todos deleted"))
//   .catch((err) => console.log(err));
// client
//   .query("DELETE FROM users")
//   .then((res) => console.log("Users deleted"))
//   .catch((err) => console.log(err));

module.exports = client;
