const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const client = require("../../db/index");
const { errorName } = require("../../error/errorConstant");

module.exports = {
  //@MUTATION :create new user
  createUser: async (args) => {
    try {
      const email = args.userInput.email;
      // Checking if the user already exist
      const user = await client.query("SELECT * from users WHERE email=$1 ", [
        email,
      ]);
      if (user.rowCount > 0) {
        throw new Error(errorName.USER_ALREADY_EXISTS);
      }
      // If user does not exist
      const hashedPassword = await bcrypt.hash(args.userInput.password, 12);
      const newUser = {
        name: args.userInput.name,
        email: args.userInput.email,
        password: hashedPassword,
      };
      // Create actual user
      const text =
        "INSERT INTO users(name, email,password) VALUES($1, $2,$3) RETURNING *";
      const values = [newUser.name, newUser.email, newUser.password];
      const createdUser = await client.query(text, values);

      return { ...createdUser.rows[0], password: null };
    } catch (err) {
      //Throw any error
      throw new Error(errorName.USER_ALREADY_EXISTS);
    }
  },
  //@QUERY: login user
  login: async ({ email, password }) => {
    try {
      // Checking if user exist
      const user = await client.query("SELECT * from users WHERE email=$1", [
        email,
      ]);
      if (user.rowCount < 1) {
        throw new Error(errorName.USER_DOESNOT_EXIST);
      }
      //Loging in user
      const isEqual = await bcrypt.compare(password, user.rows[0].password);
      if (!isEqual) {
        throw new Error(errorName.NOT_MATCHED);
      }
      // Generating Uath token
      const token = jwt.sign(
        { userId: user.rows[0].id, email: user.rows[0].email },
        "verysecretkeytogeneratetoken",
        { expiresIn: "1h" }
      );
      return { userId: user.rows[0].id, token: token, tokenExpiration: 1 };
    } catch (err) {
      // console.log(err);
      throw new Error(errorName.USER_DOESNOT_EXIST);
    }
  },
};
