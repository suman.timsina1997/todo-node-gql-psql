const client = require("../../db/index");
const { errorName } = require("../../error/errorConstant");

module.exports = {
  //@QUERY : get all to do assocoated with logged in user
  todos: async (args, req) => {
    //Checking Authentication
    if (!req.isAuth) {
      throw new Error(errorName.UNAUTHENTICATED);
    }
    const query = {
      text: "SELECT * from todos WHERE createdby=$1",
    };
    try {
      const result = await client.query(query, [req.userId]);
      return result.rows;
    } catch (err) {
      //Throw any error
      throw new Error(errorName.SERVER_ERROR);
    }
  },
  // @MUTATION :Create new todo
  createTodo: async (args, req) => {
    //Checking Authentication
    if (!req.isAuth) {
      throw new Error(errorName.UNAUTHENTICATED);
    }
    const todo = {
      title: args.todoInput.title,
      description: args.todoInput.description,
      createdat: new Date().toISOString(),
      createdby: req.userId,
    };
    // Storing query and values in constant
    const text =
      "INSERT INTO todos(title, description,createdby,createdat) VALUES($1, $2,$3,$4) RETURNING *";
    const values = [
      todo.title,
      todo.description,
      todo.createdby,
      todo.createdat,
    ];

    try {
      //Create new todo
      const result = await client.query(text, values);
      return result.rows[0];
    } catch (err) {
      //Throw any error
      throw new Error(errorName.SERVER_ERROR);
    }
  },
  //@MUTATION: Update todo
  updateTodo: async (args, req) => {
    //Checking Authentication
    if (!req.isAuth) {
      throw new Error(errorName.UNAUTHENTICATED);
    }
    try {
      //Update query with and condition and provie values for each update field
      const updatedTodo = await client.query(
        "UPDATE todos SET title=$1,description=$2  WHERE id=$3 AND createdby=$4",
        [
          args.updateTodoInput.title,
          args.updateTodoInput.description,
          args.updateTodoInput.id,
          req.userId,
        ]
      );
      return updatedTodo.rows[0];
    } catch (err) {
      //Throw any error
      throw new Error(errorName.SERVER_ERROR);
    }
  },
  //@ MUTATION: delete todo
  deleteToDo: async (args, req) => {
    try {
      //Checking Authentication
      if (!req.isAuth) {
        throw new Error(errorName.UNAUTHENTICATED);
      }
      //Delete query with condition as todo id
      const deletedTodo = await client.query("DELETE from todos where id=$1", [
        args.deleteTodoInput.id,
      ]);
    } catch (err) {
      //Throw any error
      throw new Error(errorName.SERVER_ERROR);
    }
    return `Todo Deleted with ID : ${args.deleteTodoInput.id}`;
  },
};
