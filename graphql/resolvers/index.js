// const client = require("../../db/index");

const todoResolver = require("./todos");
const userResolver = require("./users");
const RootResolver = { ...todoResolver, ...userResolver };
module.exports = RootResolver;
