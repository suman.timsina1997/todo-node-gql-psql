const { buildSchema } = require("graphql");

module.exports = buildSchema(`

type ToDo{
  id:ID!
  title:String!
  description:String!
  createdat:String!
  createdby:Int!
}
type User{
  id:ID!
  name:String!
  email:String!
  password:String
}

type AuthData{
  userId:ID!
  token:String!
  tokenExpiration:Int!
}

input TodoInput{
  title:String
  description:String!
  createdby:Int
  createdat:String
}
input UpdateTodoInput{
  id:ID!
  title:String
  description:String
}
input DeleteTodoInput{
  id:ID!
}
input UserInput{
  name:String!
  email:String!
  password:String!
}
type RootQuery{
  todos:[ToDo!]!
  login(email:String!, password:String!):AuthData!
}

type RootMutation{
  createTodo(todoInput: TodoInput):ToDo
  createUser(userInput:UserInput):User
  updateTodo(updateTodoInput:UpdateTodoInput):ToDo
  deleteTodo(deleteTodoInput:DeleteTodoInput):String
}
schema{
  query: RootQuery,
  mutation:RootMutation
}
`);
