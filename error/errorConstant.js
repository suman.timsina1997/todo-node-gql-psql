// Errorname for every possible error
exports.errorName = {
  USER_ALREADY_EXISTS: "USER_ALREADY_EXISTS",
  SERVER_ERROR: "SERVER_ERROR",
  NOT_FOUND: "NOT_FOUND",
  UNAUTHENTICATED: "UNAUTHENTICATED",
  NOT_MATCHED: "NOT_MATCHED",
  USER_DOESNOT_EXIST: "USER_DOESNOT_EXIST",
};
//  Error type for every error name with message and statuscode
exports.errorType = {
  USER_ALREADY_EXISTS: {
    message: "User already exists.",
    statusCode: 403,
  },
  SERVER_ERROR: {
    message: "Internal error.",
    statusCode: 500,
  },
  NOT_FOUND: {
    message: "Not found.",
    statusCode: 404,
  },
  UNAUTHENTICATED: {
    message: "Authentication is needed to get requested response .",
    statusCode: 401,
  },
  NOT_MATCHED: {
    message: "Email and password does not matched",
    statusCode: 401,
  },
  USER_DOESNOT_EXIST: {
    message: "User not found associated with this email",
    statusCode: 404,
  },
};
