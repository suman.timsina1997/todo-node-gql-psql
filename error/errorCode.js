const { errorType } = require("./errorConstant");

//Resolve errormessage and statusCode from errorname
const getErrorCode = (errorName) => {
  return errorType[errorName];
};

module.exports = getErrorCode;
