# Todo API with Authentication

## Install Dependencies

```
npm install
```

## Database Configuration

1. Create user "Admin" and set password "123456"
2. Create table "users" with column (id, name, email, password)
3. Create table "todos" with column (id, title, description, createdat, createdby) and set createdby as foreign key to id of users table

## Run App

```
npm start
```

---

## Some of input and response

1. Register new user
   ![Register](./images/user_created.jpg)

2. User already exist
   ![Registration_Error](./images/user_already_exist.jpg)

3. Login Success
   ![Login_Success](./images/login_success.jpg)

4. User Not found
   ![User_not_found](./images/user_not_found.jpg)

5. Authentication Required
   ![Authentication_required](./images/Authentication_required.jpg)

6. Todo Created
   ![Todo_created](./images/todo_created.jpg)

7. Todo Fetched
   ![Todo_fetched](./images/todo_fetched.jpg)
