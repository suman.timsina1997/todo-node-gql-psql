const jwt = require("jsonwebtoken");

module.exports = (req, res, next) => {
  //Checking Auth header from request
  const authHeader = req.get("Authorization");
  if (!authHeader) {
    req.isAuth = false;
    return next();
  }
  const token = authHeader.split(" ")[1];
  if (!token || token == "") {
    req.isAuth = false;
    return next();
  }
  let decodedToken;
  try {
    //Decoding token if it's present and valid
    decodedToken = jwt.verify(token, "verysecretkeytogeneratetoken");
  } catch (err) {
    req.isAuth = false;
    return next();
  }
  if (!decodedToken) {
    req.isAuth = false;
    return next();
  }
  //setting isAuth and userId variable
  req.isAuth = true;
  req.userId = decodedToken.userId;
  next();
};
