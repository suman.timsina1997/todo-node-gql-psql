const express = require("express");
const { graphqlHTTP } = require("express-graphql");
const graphqlSchema = require("./graphql/schema/index");
const graphqlResolvers = require("./graphql/resolvers/index");
const authentication = require("./middleware/authentication");
const getErrorCode = require("./error/errorCode");
const app = express();
app.use(express.json());
//Applying authentication for endpoints
app.use(authentication);

app.use("/graphql", (req, res) => {
  graphqlHTTP({
    //Providing GraphQLschema
    schema: graphqlSchema,
    graphiql: true,
    //providing GraphqlResolvers
    rootValue: graphqlResolvers,
    //Applying Custom error handling
    customFormatErrorFn: (err) => {
      console.log(err);
      const { message, statusCode } = getErrorCode(err.message);
      res.status(statusCode);
      return { statusCode, message };
    },
  })(req, res);
});
//Starting server
app.listen(3000, () => {
  console.log("App listening on port 3000!");
});
